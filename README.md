 # recomendacao-resgate

API de Recomendação de Resgate por Blocklist e Vencimento

# Tecnologias

**Maven** - Para gerenciar dependência.

**Lombok** - para produtividade, ele fica responsável por gerar os Getter e Setter; Builder; Construtor, entre outros.

**MapStruct** - Para fazer o mapper entre meu DTO e minha Classe Modelo.

**Springboot** - Com configurações rápidas, você consegue, disponibilizar uma aplicação baseada no Spring. No caso disponibilizamos uma API.

## Collection Postman
- [ ] [Recomendacao Resgate](https://www.getpostman.com/collections/32d46836addd370d7279)

## Como Utilizar

Assim que clonar o projeto para sua máquina é preciso rodar o **mvn install** para que o **mapStruct** consiga gerar a implementação do mapper automaticamente.


## Arquitetura - Clean Arch

<img src="/docs/Clean-Arch.png" alt="Clean Arch"/>

## Fluxograma

<img src="/docs/Fluxo-Resgate.drawio.png" alt="Fluxograma Resgate"/>

## Espelhamento do repositório na AWS

Este projeto tem um espelhamento com o Code Commit da AWS toda vez que um código é mergeado na branch Main.

## Espelhamento do Gitlab com Code Commit AWS
https://microfluidics.utoronto.ca/gitlab/help/user/project/repository/repository_mirroring.md#set-up-a-push-mirror-from-gitlab-to-aws-codecommit

## Observability
Na raiz do projeto tem um arquivo docker-compose.yml responsável por orquestrar container do Prometheus, Grafana e outro para a própria aplicação. 
*caso você queira subir a aplicação sem precisar startar manualmente.*

*Partindo do príncipio que você já tem o Docker instalado* **Executar**
 - docker-compose up -d

Ao executar este comando você deve obter algo como a imagem abaixo:
<img src="/docs/docker.png" alt="Docker orquestrando os containers [Prometheus, Grafana e Aplicação]" />
