package br.com.ems.recomendacaoresgate.core.usecase.impl.tiporesgate;

import br.com.ems.recomendacaoresgate.core.usecase.model.ResgateModel;

public interface Resgate {
    public Resgate regraResgate(ResgateModel resgateModel);
}
