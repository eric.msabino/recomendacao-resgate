package br.com.ems.recomendacaoresgate.core.gateway;

import java.util.List;

public interface BuscadorProdutoBlocklistGateway {

    public void buscaProdutosBlocklist(List<String> siglas);
}
