package br.com.ems.recomendacaoresgate.core.usecase.model;

import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDate;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProdutoModel {

    private String nomeProduto;

    private String siglaProduto;

    private String codigoProduto;

    private LocalDate vencimento;

    private BigDecimal valorAtual;

    private BigDecimal valorMinimoResgate;

    private Integer prazoDias;

    public String getSigla() {
        return siglaProduto.concat("@").concat(codigoProduto);
    }

    public String getNomeProduto() {
        return nomeProduto;
    }

    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    public String getSiglaProduto() {
        return siglaProduto;
    }

    public void setSiglaProduto(String siglaProduto) {
        this.siglaProduto = siglaProduto;
    }

    public String getCodigoProduto() {
        return codigoProduto;
    }

    public void setCodigoProduto(String codigoProduto) {
        this.codigoProduto = codigoProduto;
    }

    public LocalDate getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDate vencimento) {
        this.vencimento = vencimento;
    }

    public BigDecimal getValorAtual() {
        return valorAtual;
    }

    public void setValorAtual(BigDecimal valorAtual) {
        this.valorAtual = valorAtual;
    }

    public BigDecimal getValorMinimoResgate() {
        return valorMinimoResgate;
    }

    public void setValorMinimoResgate(BigDecimal valorMinimoResgate) {
        this.valorMinimoResgate = valorMinimoResgate;
    }

    public Integer getPrazoDias() {
        return prazoDias;
    }

    public void setPrazoDias(Integer prazoDias) {
        this.prazoDias = prazoDias;
    }
}
