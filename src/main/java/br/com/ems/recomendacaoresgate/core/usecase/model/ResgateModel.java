package br.com.ems.recomendacaoresgate.core.usecase.model;

import lombok.*;
import br.com.ems.recomendacaoresgate.core.usecase.enums.TipoResgateEnum;

import java.time.LocalDateTime;
import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResgateModel {

    private TipoResgateEnum tipoRecomendacaoResgate;

    private LocalDateTime dataProcessamento;

    private List<ProdutoModel> produtos;

    public TipoResgateEnum getTipoRecomendacaoResgate() {
        return tipoRecomendacaoResgate;
    }

    public void setTipoRecomendacaoResgate(TipoResgateEnum tipoRecomendacaoResgate) {
        this.tipoRecomendacaoResgate = tipoRecomendacaoResgate;
    }

    public LocalDateTime getDataProcessamento() {
        return dataProcessamento;
    }

    public void setDataProcessamento(LocalDateTime dataProcessamento) {
        this.dataProcessamento = dataProcessamento;
    }

    public List<ProdutoModel> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<ProdutoModel> produtos) {
        this.produtos = produtos;
    }
}
