package br.com.ems.recomendacaoresgate.core.usecase.enums;

public enum TipoResgateEnum {
    VENCIMENTO,
    BLOCKLIST;
}
