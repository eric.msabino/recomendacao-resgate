package br.com.ems.recomendacaoresgate.core.usecase.impl.tiporesgate;

import br.com.ems.recomendacaoresgate.core.gateway.BuscadorProdutoBlocklistGateway;
import br.com.ems.recomendacaoresgate.core.usecase.model.ResgateModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ResgateVencimentoUsecase implements Resgate {

    @Autowired
    private BuscadorProdutoBlocklistGateway produtoBlocklistGateway;

    @Override
    public Resgate regraResgate(ResgateModel resgateModel) {
        System.out.println("Entrei na Regra de Resgate por VENCIMENTO");
        return null;
    }
}
