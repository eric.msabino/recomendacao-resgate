package br.com.ems.recomendacaoresgate.core.usecase.impl;

import br.com.ems.recomendacaoresgate.core.usecase.model.ResgateModel;
import br.com.ems.recomendacaoresgate.app.adapter.entrypoint.mapper.ResgateMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import br.com.ems.recomendacaoresgate.app.adapter.entrypoint.dto.ResgateDto;
import br.com.ems.recomendacaoresgate.core.usecase.RecomendacaoResgateUsecase;
import br.com.ems.recomendacaoresgate.core.usecase.enums.TipoResgateEnum;
import br.com.ems.recomendacaoresgate.core.usecase.impl.tiporesgate.ResgateBlocklistUsecase;
import br.com.ems.recomendacaoresgate.core.usecase.impl.tiporesgate.ResgateVencimentoUsecase;

@Service
@AllArgsConstructor
public class RecomendacaoResgateUsecaseImpl implements RecomendacaoResgateUsecase {

    private static final String VENCIMENTO = "VENCIMENTO";
    private static final String BLOCKLIST = "BLOCKLIST";
    private final ResgateMapper resgateMapper;
    private final ResgateVencimentoUsecase vencimentoUsecase;
    private final ResgateBlocklistUsecase blocklistUsecase;


    @Override
    public ResgateDto executarRegraResgate(ResgateModel resgateModel) {

        if(resgateModel.getTipoRecomendacaoResgate().equals(TipoResgateEnum.VENCIMENTO)) {
            vencimentoUsecase.regraResgate(resgateModel);

        } else if(resgateModel.getTipoRecomendacaoResgate().equals(TipoResgateEnum.BLOCKLIST)) {
            blocklistUsecase.regraResgate(resgateModel);

        } else {
            blocklistUsecase.regraResgate(resgateModel);
            vencimentoUsecase.regraResgate(resgateModel);
        }

        return resgateMapper.resgateModelToResgateDto(resgateModel);
    }

}
