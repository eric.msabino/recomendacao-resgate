package br.com.ems.recomendacaoresgate.core.usecase.impl.tiporesgate;

import br.com.ems.recomendacaoresgate.core.gateway.BuscadorProdutoBlocklistGateway;
import br.com.ems.recomendacaoresgate.core.usecase.model.ResgateModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import br.com.ems.recomendacaoresgate.core.usecase.model.ProdutoModel;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ResgateBlocklistUsecase implements Resgate {
    @Autowired
    private BuscadorProdutoBlocklistGateway produtoBlocklistGateway;

    @Override
    public Resgate regraResgate(ResgateModel resgateModel) {
        System.out.println("Entrei na Regra de Resgate por BLOCKLIST");
        produtoBlocklistGateway.buscaProdutosBlocklist(getSiglas(resgateModel.getProdutos()));
        return null;
    }
    
    private List<String> getSiglas(List<ProdutoModel> produtos) {
        return produtos.stream().map(ProdutoModel::getSigla).collect(Collectors.toList());
    }
}
