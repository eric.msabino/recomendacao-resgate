package br.com.ems.recomendacaoresgate.core.usecase;

import br.com.ems.recomendacaoresgate.app.adapter.entrypoint.dto.ResgateDto;
import br.com.ems.recomendacaoresgate.core.usecase.model.ResgateModel;

public interface RecomendacaoResgateUsecase {
    public ResgateDto executarRegraResgate(ResgateModel resgateModel);
}
