package br.com.ems.recomendacaoresgate.app.adapter.dataprovider;

import br.com.ems.recomendacaoresgate.app.adapter.dataprovider.service.BuscadorProdutoBlocklistService;
import br.com.ems.recomendacaoresgate.core.gateway.BuscadorProdutoBlocklistGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BuscadorProdutoBlocklistDataprovider implements BuscadorProdutoBlocklistGateway {

    @Autowired
    private BuscadorProdutoBlocklistService produtoBlocklistService;

    @Override
    public void buscaProdutosBlocklist(List<String> siglas) {
        produtoBlocklistService.integracaoProduto(siglas);
    }
}
