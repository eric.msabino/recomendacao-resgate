package br.com.ems.recomendacaoresgate.app.configuration.metrics;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class RecomendacaoResgateMetrics {

    @Autowired
    private Metrics metrics;

    public void contabilizaRecomendacaoResgateProcessado(String tipoProcessamento) {
        try {
            metrics.counterMetrics(
                    "recomendacao_resgate",
                    "tipo-processamento", tipoProcessamento
            );
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }
}
