package br.com.ems.recomendacaoresgate.app.adapter.entrypoint.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDate;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProdutoDto {

    @JsonProperty("nome_produto")
    private String nomeProduto;

    @JsonProperty("sigla_produto")
    private String siglaProduto;

    @JsonProperty("codigo_produto")
    private String codigoProduto;

    @JsonProperty("vencimento")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate vencimento;

    @JsonProperty("valor_atual")
    private BigDecimal valorAtual;

    @JsonProperty("valor_minimo_resgate")
    private BigDecimal valorMinimoResgate;

    @JsonProperty("prazo_dias")
    private Integer prazoDias;

    public String getNomeProduto() {
        return nomeProduto;
    }

    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    public String getSiglaProduto() {
        return siglaProduto;
    }

    public void setSiglaProduto(String siglaProduto) {
        this.siglaProduto = siglaProduto;
    }

    public String getCodigoProduto() {
        return codigoProduto;
    }

    public void setCodigoProduto(String codigoProduto) {
        this.codigoProduto = codigoProduto;
    }

    public LocalDate getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDate vencimento) {
        this.vencimento = vencimento;
    }

    public BigDecimal getValorAtual() {
        return valorAtual;
    }

    public void setValorAtual(BigDecimal valorAtual) {
        this.valorAtual = valorAtual;
    }

    public BigDecimal getValorMinimoResgate() {
        return valorMinimoResgate;
    }

    public void setValorMinimoResgate(BigDecimal valorMinimoResgate) {
        this.valorMinimoResgate = valorMinimoResgate;
    }

    public Integer getPrazoDias() {
        return prazoDias;
    }

    public void setPrazoDias(Integer prazoDias) {
        this.prazoDias = prazoDias;
    }
}
