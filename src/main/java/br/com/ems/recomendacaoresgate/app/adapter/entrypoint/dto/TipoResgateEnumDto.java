package br.com.ems.recomendacaoresgate.app.adapter.entrypoint.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum TipoResgateEnumDto {
    VENCIMENTO("Vencimento"),
    BLOCKLIST("Blocklist");
//    TODOS("TODOS");

    private String tipo;
    TipoResgateEnumDto(String tipo) {
        this.tipo = tipo;
    }
}
