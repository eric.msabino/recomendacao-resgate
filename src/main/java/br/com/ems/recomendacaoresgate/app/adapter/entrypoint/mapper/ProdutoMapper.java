package br.com.ems.recomendacaoresgate.app.adapter.entrypoint.mapper;

import br.com.ems.recomendacaoresgate.app.adapter.entrypoint.dto.ProdutoDto;
import br.com.ems.recomendacaoresgate.core.usecase.model.ProdutoModel;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProdutoMapper {

    public ProdutoModel produtoDtoToProdutoModel(ProdutoDto produtoDto);

    @InheritInverseConfiguration
    ProdutoDto produtoModelToProdutoDto(ProdutoModel produtoModel);
}
