package br.com.ems.recomendacaoresgate.app.configuration.metrics;

import io.micrometer.core.instrument.Meter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.config.MeterFilter;
import io.micrometer.core.instrument.distribution.DistributionStatisticConfig;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

import static io.micrometer.core.instrument.Meter.Type.TIMER;

@Configuration
@Slf4j
public class MicrometerConfig {

    @Bean
    public MeterRegistryCustomizer<MeterRegistry> exposeHistogramInfo() {
        return registry -> registry.config().meterFilter(new MeterFilter() {
            @Override
            public DistributionStatisticConfig configure(@NonNull Meter.Id id,
                                                         @NonNull DistributionStatisticConfig config) {
                final var meterTyNameRegex = "^(http|hystrix){1}.*";
                final var shouldPopulateStastistics = id.getType() == TIMER && id.getName().matches(meterTyNameRegex);

                if(!shouldPopulateStastistics)
                    return config;

                return DistributionStatisticConfig.builder()
                        .percentilesHistogram(true)
                        .percentiles(0.5, 0.90, 0.95, 0.99)
                        .sla(Duration.ofMillis(50).toNanos(),
                             Duration.ofMillis(100).toNanos(),
                             Duration.ofMillis(200).toNanos(),
                             Duration.ofSeconds(1).toNanos(),
                             Duration.ofSeconds(5).toNanos()
                        )
                        .minimumExpectedValue(Duration.ofMillis(1).toNanos())
                        .maximumExpectedValue(Duration.ofSeconds(5).toNanos())
                        .build()
                        .merge(config);
            }
        });
    }
}
