package br.com.ems.recomendacaoresgate.app.adapter.entrypoint.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.time.LocalDateTime;
import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResgateDto {

    @JsonProperty("tipo_recomendacao_resgate")
    @Enumerated(EnumType.STRING)
    //@Pattern(regexp = "VENCIMENTO|BLOCKLIST|TODOS", flags = Pattern.Flag.CASE_INSENSITIVE, message = "tipo_recomendacao_resgate precisa ser [VENCIMENTO, BLOCKLIST ou TODOS]")
    private TipoResgateEnumDto tipoRecomendacaoResgate;

    @Null(message = "Data processamento deve ser inserida pelo sistema")
    @JsonProperty("data_processamento")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private LocalDateTime dataProcessamento;

    @NotNull(message = "É necessário ao menos um produto na lista para recomendação de resgate")
    @NotEmpty(message = "É necessário ao menos um produto na lista para recomendação de resgate")
    @JsonProperty("produtos")
    private List<ProdutoDto> produtos;

    public TipoResgateEnumDto getTipoRecomendacaoResgate() {
        return tipoRecomendacaoResgate;
    }

    public void setTipoRecomendacaoResgate(TipoResgateEnumDto tipoRecomendacaoResgate) {
        this.tipoRecomendacaoResgate = tipoRecomendacaoResgate;
    }

    public LocalDateTime getDataProcessamento() {
        return dataProcessamento;
    }

    public void setDataProcessamento(LocalDateTime dataProcessamento) {
        this.dataProcessamento = dataProcessamento;
    }

    public List<ProdutoDto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<ProdutoDto> produtos) {
        this.produtos = produtos;
    }
}
