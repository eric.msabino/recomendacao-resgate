package br.com.ems.recomendacaoresgate.app.adapter.entrypoint.mapper;

import br.com.ems.recomendacaoresgate.app.adapter.entrypoint.dto.ResgateDto;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import br.com.ems.recomendacaoresgate.core.usecase.model.ResgateModel;


@Mapper(componentModel = "spring", uses = ProdutoMapper.class)
public interface ResgateMapper {

    @Mappings({
            @Mapping(target = "dataProcessamento", expression = "java(java.time.LocalDateTime.now())")
    })
    ResgateModel resgateDtoToResgateModel(ResgateDto resgateDto);

    @InheritInverseConfiguration
    ResgateDto resgateModelToResgateDto(ResgateModel resgateModel);

}
