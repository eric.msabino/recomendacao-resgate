package br.com.ems.recomendacaoresgate.app.adapter.dataprovider.service;


import br.com.ems.recomendacaoresgate.app.configuration.metrics.RecomendacaoResgateMetrics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BuscadorProdutoBlocklistServiceImpl implements BuscadorProdutoBlocklistService {

    private static final String SUCESSO = "SUCESSO";
    private static final String ERRO = "ERRO";

    @Autowired
    private RecomendacaoResgateMetrics metrics;

    @Override
    public void integracaoProduto(List<String> siglas) {
        //Fazer Mock de produto Blocklist e alterar retorno do método
        try {
            for (int i = 0; i < 10; i++) {
                metrics.contabilizaRecomendacaoResgateProcessado(SUCESSO);
            }
            for (int i = 0; i < 5; i++) {
                metrics.contabilizaRecomendacaoResgateProcessado(ERRO);
            }
        } catch (Exception e) {
            metrics.contabilizaRecomendacaoResgateProcessado(ERRO);
        }
    }
}
