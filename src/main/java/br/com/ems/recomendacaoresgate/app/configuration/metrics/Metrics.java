package br.com.ems.recomendacaoresgate.app.configuration.metrics;

import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.stereotype.Component;

@Component
public class Metrics {

    private final MeterRegistry meterRegistry;

    public Metrics(MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
    }

    public void counterMetrics(final String metricName, final String... tags) {
        meterRegistry.counter(metricName, tags).increment();
    }
}
