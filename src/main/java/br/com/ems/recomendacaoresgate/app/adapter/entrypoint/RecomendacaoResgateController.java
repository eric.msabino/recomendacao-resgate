package br.com.ems.recomendacaoresgate.app.adapter.entrypoint;

import br.com.ems.recomendacaoresgate.app.adapter.entrypoint.mapper.ResgateMapper;
import br.com.ems.recomendacaoresgate.app.adapter.entrypoint.dto.ResgateDto;
import br.com.ems.recomendacaoresgate.core.usecase.RecomendacaoResgateUsecase;
import br.com.ems.recomendacaoresgate.core.usecase.model.ResgateModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class RecomendacaoResgateController {

    private final ResgateMapper resgateMapper;

    private RecomendacaoResgateUsecase recomendacaoResgateUsecase;

    @Autowired
    public RecomendacaoResgateController(ResgateMapper resgateMapper, RecomendacaoResgateUsecase recomendacaoResgateUsecase) {
        this.resgateMapper = resgateMapper;
        this.recomendacaoResgateUsecase = recomendacaoResgateUsecase;
    }

    @PostMapping(value = "/v1/recomendacoes_resgate")
    public ResponseEntity gerarRecomendacaoResgate(@Valid @RequestBody ResgateDto resgateRequestDto) {
        ResgateModel resgateModel = resgateMapper.resgateDtoToResgateModel(resgateRequestDto);

        return ResponseEntity.ok().body(recomendacaoResgateUsecase.executarRegraResgate(resgateModel));
    }
}
